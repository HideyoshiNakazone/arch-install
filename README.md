Simple personal shell script for automatic installation of arch using the gnome desktop, btrfs filesystem, plymouth, uswsusp and snapper.

It's usage is simple:

`cd arch-install`

`./Install [host name] [username]:[passwd]`

This will start the installation process in your system settings the host name to `[host name]`, creating the user with the name passed by `[username]`, and setting both user and root password to `[passwd]`. By personal choice the Desktop Environment is Gnome, but you are free to edit any part of the script under the GPL v2.

Thanks for the preference and have fun with Arch Linux!